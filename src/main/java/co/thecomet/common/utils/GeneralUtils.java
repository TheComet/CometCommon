package co.thecomet.common.utils;

import java.util.Random;

public class GeneralUtils {
    public static final Random RANDOM = new Random(System.currentTimeMillis());

    public static int getBetween(int min, int max) {
        return RANDOM.nextInt(max - min) + min;
    }
}
