package co.thecomet.common.async;

import java.util.concurrent.Callable;

public abstract class Callback<V> implements Callable<V> {
    public abstract V call();
}
