package co.thecomet.common.async;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Async {
    private static ExecutorService executorService = Executors.newFixedThreadPool(5);
    
    public static void execute(Runnable runnable) {
        executorService.execute(runnable);
    }
    
    public static Future<?> submit(Runnable runnable) {
        return executorService.submit(runnable);
    }
    
    public static <T> Future<T> submit(Callable<T> task) {
        return executorService.submit(task);
    }
    
    public static <T> Future<T> submit(Runnable runnable, T result) {
        return executorService.submit(runnable, result);
    }
}
